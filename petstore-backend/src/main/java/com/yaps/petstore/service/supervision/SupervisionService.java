package com.yaps.petstore.service.supervision;

import com.yaps.petstore.common.exception.CheckException;
import com.yaps.petstore.model.supervision.ESVersionDTO;
import com.yaps.petstore.model.supervision.VersionDTO;

public interface SupervisionService {

	/**
	 * 
	 * @param versionId
	 * @return
	 * @throws CheckException
	 */
	public abstract VersionDTO findVersion(Long versionId)
			throws CheckException;

	
	
	/**
	 * 
	 * @return
	 * @throws CheckException
	 */
	public abstract VersionDTO findVersion()
			throws CheckException;
	

	
	/**
	 * 
	 * @return
	 * @throws CheckException
	 */
	public abstract ESVersionDTO elasticSearchVersion()
			throws CheckException;
	
	
}