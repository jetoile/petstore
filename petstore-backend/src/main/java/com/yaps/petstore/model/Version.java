package com.yaps.petstore.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * This class encapsulates all the data for a Version.
 *
 */
public  class Version implements AbstractBean {

    /**
	 * 
	 */
	private static final long serialVersionUID = 4648729478877163459L;
	// ======================================
    // =             Attributes             =
    // ======================================

	private Long id;
    private String name;


    // ======================================
    // =            Constructors            =
    // ======================================
    public Version() {
    }

    public Version(final Long id, final String name) {
        this.id = id;
        this.name = name;
    }

    // ======================================
    // =         Getters and Setters        =
    // ======================================
    
    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
    	this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
    	this.name = name;
    }


	public String toString() {
		return ToStringBuilder.reflectionToString(this);
    }
	
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	
	public int hashCode(Object obj) {
		return new HashCodeBuilder().append(id) .append(name).hashCode();
	}	
	
}
