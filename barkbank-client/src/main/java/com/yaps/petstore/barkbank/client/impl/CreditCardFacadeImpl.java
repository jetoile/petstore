package com.yaps.petstore.barkbank.client.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.barkbank.v1.CreditCard;
import com.barkbank.v1.CreditCardPort;
import com.barkbank.v1.CreditCardRequest;
import com.barkbank.v1.CreditCardResponse;
import com.yaps.petstore.barkbank.client.CreditCardFacade;
import com.yaps.petstore.common.exception.CheckException;
import com.yaps.petstore.model.CreditCardDTO;

public class CreditCardFacadeImpl implements CreditCardFacade {
	
	Logger logger = LoggerFactory.getLogger(CreditCardFacadeImpl.class.getName());
	
	
	private com.barkbank.v1.CreditCardPort creditCardService;

	public CreditCardPort getCreditCardService() {
		return creditCardService;
	}

	public void setCreditCardService(CreditCardPort creditCardService) {
		this.creditCardService = creditCardService;
	}
	
	
	public String verifyCreditCard(CreditCardDTO creditCardDTO) throws CheckException {

		logger.debug("start verifyCreditCard");
		
		CreditCard creditCard = new CreditCard();
		
		creditCard.setCreditCardExpiryDate(creditCardDTO.getCreditCardExpiryDate());
		creditCard.setCreditCardNumber(creditCardDTO.getCreditCardNumber());
		creditCard.setCreditCardType(creditCardDTO.getCreditCardType());
		

		CreditCardRequest request = new CreditCardRequest();
		request.setCreditCard(creditCard);
		CreditCardResponse response = creditCardService.verifyOperation(request);

		logger.debug("response status {}",response.getReturn());
		logger.debug("end verifyCreditCard");		
		
		return response.getReturn();
	}

}
