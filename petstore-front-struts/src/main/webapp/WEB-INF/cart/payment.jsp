<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>


                <TABLE cellSpacing=1 cellPadding=1 width="100%" border=1>
                    <TR>
                        <TD>
                            <TABLE   cellSpacing=1 cellPadding=1 width="100%" border="1">
                                        <tr valign="top" >
                                            <td>
												Item Name												
                                            </td>
                                            <td>
												Quantity
                                            </td>
                                            <td>
                                            	Unit Cost (Euros)
                                            </td>
                                            <td>
                                            	Total cost per item                                            
                                            </td>
                                        </tr>

                                <%-- Lists all the items in the shopping cart --%>
                                <s:iterator value="finalCartDto.cartItems" id="cartItemDTO" >
                                    <s:form  method="POST" theme="simple">
                                        <tr valign="top" >
                                            <td>
												<s:url id="findItemUrl" action="item" method="findItem">
													<s:param name="itemId">
														<s:property value="#cartItemDTO.itemId" />
													</s:param>
												</s:url>
												<s:a href="%{findItemUrl}"><s:property value="#cartItemDTO.productDescription" /></s:a><br/>												
												
                                            </td>
                                            <td>
                                            	<s:property  value="%{#cartItemDTO.quantity}" />
                                            	<s:hidden name="itemId"  label="%{shoppingCartItem.itemId}"  value="%{#cartItemDTO.itemId}"/> 
                                            </td>
                                            <td>
                                            	<s:property value="#cartItemDTO.unitCost"/>
                                            </td>
                                            <td>
                                            	<s:property value="#cartItemDTO.totalCost"/>                                            
                                            </td>
                                        </tr>
                                    </s:form>
                                </s:iterator>

                                <%-- Total --%>
                                <tr>
                                    <td colspan="7" align="right">
                                        <b>Total:</b>
                                    </td>
                                    <td bgcolor="#CCCCFF" align="right">
                                        <s:property value="%{finalCartDto.total}"/>
                                    </td>
                                </tr>
                            </table>
                        </TD>
                    </TR>
                </table>
                
                <p>
                <h1>Credit Card</h1>
                </p>
                <p align="left"> 
                <table  border=1 >
                	<s:form theme="simple">
                	<tr>
                		<td>Credit Card number</td>
                		<td><s:textfield  name="creditCardNumber"/></td>                		
                	</tr>
                	<tr>
                		<td>Credit Card type</td>
                		<td><s:textfield  name="creditCardType" /></td>                		
                	</tr>
                	<tr>
                		<td>Credit Card expiry date</td>
                		<td><s:textfield  name="creditCardExpiryDate" /></td>                		
                	</tr>
                	<tr>
                		<td colspan="2" align="right"><s:submit action="shopping-cart-pay" method="pay" value="payment"/></td>                		
                	</tr>
                	<tr>
                		<td colspan="2" align="right"><s:submit value="cancel" /></td>                		
                	</tr>                	                	                	                	
                	</s:form>
                </table>
                </p>
