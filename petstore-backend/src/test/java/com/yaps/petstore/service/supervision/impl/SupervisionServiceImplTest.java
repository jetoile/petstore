package com.yaps.petstore.service.supervision.impl;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.yaps.petstore.common.exception.CheckException;
import com.yaps.petstore.dao.VersionDao;
import com.yaps.petstore.model.Version;
import com.yaps.petstore.model.supervision.VersionDTO;


@RunWith(MockitoJUnitRunner.class)
public class SupervisionServiceImplTest {

	Long id = Long.valueOf(1);
	
	@Mock
	private VersionDao versionDaoMock;	


	@Rule
	public ExpectedException exception = ExpectedException.none();
	

	private SupervisionServiceImpl supervisionService;
	
	
	@Before
	public void setUp() throws Exception {
		
		supervisionService = new SupervisionServiceImpl();
		supervisionService.setVersionDao(versionDaoMock);
		
		Mapper mapper = new DozerBeanMapper();
		supervisionService.setMapper(mapper);
		
	}	

	@After
	public void tearDown() throws Exception {
		supervisionService = null;
	}
	
	
	
	@Test
	public void testFindCustomerByEmail() throws Exception {
		Long id = Long.valueOf(1);

		Version version = getVersion(id);

		Mapper mapper = new DozerBeanMapper();
		supervisionService.setMapper(mapper);
		
		
		VersionDTO expectedVersion = mapper.map(version, VersionDTO.class);
		
		when((versionDaoMock).get(id)).thenReturn(version);

		VersionDTO versDto = supervisionService.findVersion(id);
		
		verify(versionDaoMock).get(id);
		assertEquals(expectedVersion,versDto);
	}


	@Test 
	public void findCurrentVersionTest() throws Exception {

		Version version = getVersion(id);
		List<Version> versions = new ArrayList<Version>();
		versions.add(version);
		
		
		when(versionDaoMock.findAll()).thenReturn(versions);

		Mapper mapper = new DozerBeanMapper();
		supervisionService.setMapper(mapper);
		VersionDTO expectedVersion = mapper.map(version, VersionDTO.class);
		
		
		VersionDTO versDto = supervisionService.findVersion();
		
		verify(versionDaoMock).findAll();
		assertEquals(expectedVersion,versDto);
	}	
	
	
	@Test
	public void toManyVersionsTest() throws Exception {
		
		exception.expect(CheckException.class);
		exception.expectMessage(containsString(SupervisionServiceImpl.UNCORRECT_VERSIONS));
		
		Version version1 = getVersion(id);
		Version version2 = getVersion(Long.valueOf(2));
		List<Version> versions = new ArrayList<Version>();
		versions.add(version1);
		versions.add(version2);
		
		
		when(versionDaoMock.findAll()).thenReturn(versions);

		Mapper mapper = new DozerBeanMapper();
		supervisionService.setMapper(mapper);
		
		supervisionService.findVersion();
		
	}
	
	
	
	@Test
	public void toNotEnoughVersionsTest() throws Exception {
		
		exception.expect(CheckException.class);
		exception.expectMessage(containsString(SupervisionServiceImpl.UNCORRECT_VERSIONS));
		
		when(versionDaoMock.findAll()).thenReturn(null);

		Mapper mapper = new DozerBeanMapper();
		supervisionService.setMapper(mapper);
		
		supervisionService.findVersion();
		
	}	
	
	
	private Version getVersion(Long id) {
		Version version = new Version(id,"DB-VERSION 1.0");
		return version;
	}	
	
}
