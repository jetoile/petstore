package com.yaps.petstore.web.rest.supervision;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import javax.servlet.ServletContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yaps.petstore.common.exception.CheckException;
import com.yaps.petstore.model.supervision.ESVersionDTO;
import com.yaps.petstore.model.supervision.VersionDTO;
import com.yaps.petstore.service.supervision.SupervisionService;

@Controller
@Scope("request")
@RequestMapping("/lifeline")
public class LifeLine {

	Logger logger = LoggerFactory.getLogger(LifeLine.class.getName());

	String instance;

	String version;
	
	String buildTimeStamp;	

	@Autowired
	ServletContext servletContext;

	@Autowired 
	SupervisionService supervisionService;
	
	
	public String getVersion() {
		return version;
	}
	
	@Value("#{appProperties['project.admin.version']}")
	public void setVersion(String version){
		this.version = version;
	}
	
	
	@Value("#{appProperties['build.time.stamp']}")
	public void setBuildTimeStamp(String buildTimeStamp){
		this.buildTimeStamp = buildTimeStamp;
	}
	
	public String getBuildTimeStamp(String version){
		return buildTimeStamp;
	}	
	

	public String getInstance() {
		return instance;
	}


	@Value("#{appProperties['admin.load.balancer.instance']}")	
	public void setInstance(String instance) {
		this.instance = instance;
	}

	public void setServletContext(ServletContext aServletContext) {
		this.servletContext = aServletContext;
	}

	@RequestMapping(value = "/light", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody
	ResponseEntity<String> checkLight() {
		return new ResponseEntity<String>("OK", HttpStatus.OK);
	}

	
	@RequestMapping(value = "/db", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody VersionDTO checkSGBD() {
		VersionDTO result = null;
		try {
			result =  supervisionService.findVersion();
		} catch (CheckException e) {
			logger.error(e.getMessage(),e);
		}
		
		return result;
	}
	
	
	@RequestMapping(value = "/es", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ESVersionDTO checkElasticSearch() {
		ESVersionDTO result = null;
		try {
			result =  supervisionService.elasticSearchVersion();
		} catch (CheckException e) {
			logger.error(e.getMessage(),e);
		}
		
		return result;
	}	
	
	
	@RequestMapping(value = "/full", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody
	ResponseEntity<String> checkInstance() {

		File file = new File(instance);
		FileReader namereader = null;
		try {
			namereader = new FileReader(file);
		} catch (FileNotFoundException e) {
			logger.error(e.getMessage(),e);
		}
		
		BufferedReader in = new BufferedReader(namereader);
		String instanceStatus;
		
		//default response is OK to avoid side effect
		ResponseEntity<String> result = new ResponseEntity<String>("OK", HttpStatus.OK);
		
		try {
			instanceStatus = in.readLine();
			if (SERVER_STATUS.KO.toString().equals(instanceStatus)) {
				//Send a not found response
				result =  new ResponseEntity<String>(HttpStatus.NOT_FOUND);
			} 
		} catch (IOException e) {
			logger.error(e.getMessage(),e);
		} finally {
			try {
				if(in != null){
					in.close();
				}
			} catch (IOException e){
				logger.error(e.getMessage(),e);				
			}
		}

		return result;

	}

	@RequestMapping(value = "/admin/version", method = RequestMethod.GET, produces = "text/plain")
	public @ResponseBody String adminVersion() {
		return new StringBuilder()
			.append("Version : ")
			.append(version)
			.append(" - ")			
			.append("build time stamp : ") 
			.append(buildTimeStamp).toString()			
			;
	}
	
	
	@RequestMapping(value = "/dependencies", method = RequestMethod.GET)
	public @ResponseBody String checkDependencies()  {

		// ClassLoader cl = ClassLoader.getSystemClassLoader();
		//
		// URL[] urls = ((URLClassLoader)cl).getURLs();
		//
		// StringBuilder builder = new StringBuilder();
		//
		// for(URL url: urls){
		// builder.append(url.getFile()).append("\n") ;
		// }

		// Properties prop = new Properties();
		// prop.load(servletContext.getResourceAsStream("/META-INF/MANIFEST.MF"));
		//
		// return prop.getProperty("Class-Path");
		
		//get property from bean
		//Properties props = appContext.getBean("appProperties", Properties.class);

		StringBuilder builder = new StringBuilder();

		String libPath = servletContext.getRealPath("WEB-INF/lib");
		File file = new File(libPath);
		builder.append("<ul>");

		if (file.isDirectory()) {
			for (File child : file.listFiles()) {
				builder.append("<li>").append(child.getName()).append("</li>");
			}
		}
		builder.append("</ul>");

		return builder.toString();
	}

}