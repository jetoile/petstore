<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<table cellspacing="0" width="100%">
    <tr>
        <td align="left" valign="middle">
    		<a href="<%= request.getContextPath() %>/"><img border="0" src="images/banner_logo.gif"/></a>
        </td>
        <td align="right" valign="middle">
            <s:form action="items" method="searchItems">
                <s:textfield key="keyword" name="keyword" />
                <s:submit  value="Search" />
            </s:form>
            <br>
            
            
				<s:url id="signOnUrl" action="customer-sign-on-form" method="signOnViewForm"/>
				<s:url id="signOffUrl" action="customer-sign-off" method="signOff"/>

				<s:url id="findAccountUrl" action="customer-find" method="findAccount"/>
				<s:url id="findShoppingCartUrl" action="shopping-cart-find" method="findShoppingCart"/>				            
            
            	<s:if test="#session.customer == null">
                    <s:a href="%{signOnUrl}">Sign On</s:a>
            	</s:if>
				<s:else>
					welcome  : <s:property  value="#session.customer.firstname"/> <s:property  value="#session.customer.lastname"/> !<br/>
                	<s:a href="%{findAccountUrl}">Account</s:a>
                	<s:a href="%{findShoppingCartUrl}">Cart</s:a>                	
                    <s:a href="%{signOffUrl}">Sign Off</s:a>
				</s:else>

        </td>
    </tr>
</table>

<table cellspacing="0" width="100%">
    <tr>
        <td>
            <hr noShade SIZE=1>
        </td>
    </tr>
</table>
