package com.yaps.petstore.web.action;

import static com.yaps.petstore.web.utils.SessionUtils.CUSTOMER_SESSION;
import static com.yaps.petstore.web.utils.SessionUtils.SHOPPING_CART;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.interceptor.RequestAware;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.yaps.petstore.common.exception.CheckException;
import com.yaps.petstore.common.exception.NotFoundException;
import com.yaps.petstore.model.CustomerDTO;
import com.yaps.petstore.model.OrderDTO;
import com.yaps.petstore.service.customer.CustomerService;
import com.yaps.petstore.service.order.OrderService;

public class CustomerAction extends ActionSupport implements ModelDriven<CustomerDTO>, ServletRequestAware, RequestAware {


	/**
	 * 
	 */
	private static final long serialVersionUID = -6667876554000932726L;


	Logger logger = LoggerFactory.getLogger(getCname());
	
	
	private CustomerDTO customerDto = new CustomerDTO();
	
	private String password2;
	
	@Autowired
	private CustomerService customerService;
	
	@Autowired	
	private OrderService orderService;	

	private Map<String,Object> map;
	
	private List<OrderDTO> orders;
	
	private HttpServletRequest httpServeltRequest;
	
	@Override
	public CustomerDTO getModel() {
		return customerDto;
	}

	public CustomerDTO getCustomerDto(){
		return customerDto;
	}
	
	public void setCustomerDto(CustomerDTO customerDto) {
		this.customerDto = customerDto;
	}

	
	public List<OrderDTO> getOrders() {
		return orders;
	}

	public void setOrders(List<OrderDTO> orders) {
		this.orders = orders;
	}

	public void setPassword2(String password2) {
		this.password2 = password2;
	}

	public String getPassword2(){
		return password2;
	}
	
	
	public CustomerService getCustomerService() {
		return customerService;
	}


	public void setCustomerService(CustomerService customerService) {
		this.customerService = customerService;
	}

	
	
	
	public OrderService getOrderService() {
		return orderService;
	}

	public void setOrderService(OrderService orderService) {
		this.orderService = orderService;
	}

	public String signOnViewForm() throws Exception {

		logger.info("start signOnViewForm");
		
		return SUCCESS;
	}	
	
	
	public String findAccount() throws Exception {

		logger.info("start findAccount");

		getCustomer();
		
		setOrders(orderService.findOrders(customerDto.getId()));
		
		logger.info("fin findAccount");
		
		return SUCCESS;

	}

	
	public String signOn() throws Exception {

		logger.info("start signOn");

		getCustomer();
		
		logger.info("fin signOn");
		
		return SUCCESS;

	}
	
	
	
	
	public String edit() throws Exception {

		logger.info("start edit");

		customerDto = customerService.findCustomer(customerDto.getId());
				
		logger.info("fin edit");
		
		return SUCCESS;

	}

	

	public String createForm() throws Exception {
		logger.info(" debut createForm");

		logger.info("customerDTO.pass = {}",customerDto.getPassword());
		logger.info("customerDTO.pass2 = {}",getPassword2());
		logger.info("customerDTO.email = {}",customerDto.getEmail());		
		
		
		try {
			CustomerDTO  aCustomerDto = customerService.findCustomerById(customerDto.getEmail());
			
			map.put("pass", aCustomerDto.getPassword());
			map.put("pass2", getPassword2());
			map.put("emailId", aCustomerDto.getEmail());		
			
			
		} catch (NotFoundException e){
			logger.warn("expected exception");
		}

		
		
		return SUCCESS;
	}	
	
	
	public String create() throws Exception {
		logger.info(" debut create");

		customerDto = customerService.createCustomer(customerDto);

		this.httpServeltRequest.getSession().setAttribute(CUSTOMER_SESSION, customerDto);

		return SUCCESS;
	}

	public String update() throws Exception {
		logger.info(" debut update");

		customerService.updateCustomer(customerDto);

		customerDto = customerService.findCustomer(customerDto.getId());

		this.httpServeltRequest.getSession().setAttribute(CUSTOMER_SESSION, customerDto);
		
		logger.info(" debut update");
		return SUCCESS;

	}	
	
	
	public String signOff() throws Exception {
		logger.info(" debut signOff");

		this.httpServeltRequest.getSession().removeAttribute(CUSTOMER_SESSION);
		this.httpServeltRequest.getSession().removeAttribute(SHOPPING_CART);		
		
		return SUCCESS;
	}
	
	

	protected String getCname() {
		return this.getClass().getName();
	}


	@Override
	public void setServletRequest(HttpServletRequest httpServeltRequest) {
		this.httpServeltRequest = httpServeltRequest;
	}


	@Override
	public void setRequest(Map<String, Object> arg0) {
		this.map = arg0;
	}

//	public void validate(){
//	    if ( customerDto.getPassword() != getPassword2()){
//	        addFieldError( "customerDto.password", "Password error" );
//	    }
//	}

	// ******************* Private Methods ******************* //
	private void getCustomer() throws CheckException {
		logger.info("customerDTO.password = {}",customerDto.getPassword());
		logger.info("customerDTO.email = {}",customerDto.getEmail());		

		
		CustomerDTO aCustomerDto = (CustomerDTO)this.httpServeltRequest.getSession().getAttribute(CUSTOMER_SESSION);
		
		if (aCustomerDto == null){
			aCustomerDto = customerService.authenticate(customerDto.getEmail(), customerDto.getPassword());
			this.httpServeltRequest.getSession().setAttribute(CUSTOMER_SESSION, aCustomerDto);
		} 
		
		//Always set the customerDto 
		setCustomerDto(aCustomerDto);
	}	
	
}