<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>


			<s:if test="%{orders != null }">
				Customer orders
				<table>
                <s:iterator value="orders" id="order" >
					<tr>
						<td>Your order id : </td>
						<td><s:property value="id"/></td>
					</tr>
					<tr>
						<td colspan="2">
						<table>
						 	<s:iterator value="orderLines" id="orderline" >
						 		<tr>
									<td>id</td>
									<td><s:property value="id"/></td>									
						 		</tr>
						 		<tr>
									<td>quantity</td>
									<td><s:property value="quantity"/></td>									
						 		</tr>						 		
						 	</s:iterator>
						</table>
						<td>
					</tr>
                </s:iterator>
                </table>				
			</s:if>
			<<s:else>
				No order for this customer yet ...
			</s:else>
