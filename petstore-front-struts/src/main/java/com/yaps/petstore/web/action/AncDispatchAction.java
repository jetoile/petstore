package com.yaps.petstore.web.action;





public abstract class AncDispatchAction   {


    // Used for logging
    protected final transient String _cname = this.getClass().getName();

    //protected transient Logger logger = Logger.getLogger(_cname);    
    
    // ======================================
    // =         Getters and Setters        =
    // ======================================
    protected abstract String getCname() ;
}
