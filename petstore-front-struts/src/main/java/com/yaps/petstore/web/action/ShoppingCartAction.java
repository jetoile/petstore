package com.yaps.petstore.web.action;

import static com.yaps.petstore.web.utils.SessionUtils.CUSTOMER_SESSION;
import static com.yaps.petstore.web.utils.SessionUtils.SHOPPING_CART;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.interceptor.ServletRequestAware;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.yaps.petstore.model.CustomerDTO;
import com.yaps.petstore.model.FinalCartDTO;
import com.yaps.petstore.model.OrderDTO;
import com.yaps.petstore.model.ShoppingCartItemDTO;
import com.yaps.petstore.service.cart.ShoppingCartService;
import com.yaps.petstore.service.order.OrderService;


public class ShoppingCartAction extends ActionSupport implements ServletRequestAware, ModelDriven<ShoppingCartItemDTO>{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7200255001194006321L;

	private HttpServletRequest httpServeltRequest;	
	
	Logger logger = LoggerFactory.getLogger(getCname());

	protected FinalCartDTO finalCartDto;
	
	@Autowired
	protected ShoppingCartService cartService;
	
	@Autowired
	protected OrderService orderService;	

	private ShoppingCartItemDTO shoppingCartItem = new ShoppingCartItemDTO();
	
	private List<OrderDTO> orders;
	
	protected String getCname() {
		return this.getClass().getName();
	}

	
	public ShoppingCartService getCartService() {
		return cartService;
	}


	public void setCartService(ShoppingCartService cartService) {
		this.cartService = cartService;
	}

	
	public OrderService getOrderService() {
		return orderService;
	}

	public void setOrderService(OrderService orderService) {
		this.orderService = orderService;
	}

	public FinalCartDTO getFinalCartDto() {
		return finalCartDto;
	}


	public void setFinalCartDto(FinalCartDTO finalCartDto) {
		this.finalCartDto = finalCartDto;
	}

	public List<OrderDTO> getOrders() {
		return orders;
	}

	public void setOrders(List<OrderDTO> orders) {
		this.orders = orders;
	}
	
	

	public String addItem(){
		logger.debug("start addItem ");
		
		String itemId = this.httpServeltRequest.getParameter("itemId");
		logger.debug(" itemId {}",itemId);
		
		Map<Long,Integer> sessionCart = (Map<Long,Integer>)this.httpServeltRequest.getSession().getAttribute(SHOPPING_CART);
		
		if (sessionCart == null){
			sessionCart = new HashMap<Long, Integer>();
			sessionCart.put(Long.valueOf(itemId),Integer.valueOf(1));
			this.httpServeltRequest.getSession().setAttribute(SHOPPING_CART, sessionCart);
		} else {
			sessionCart.put(Long.valueOf(itemId),Integer.valueOf(1));
		}
		
		logger.debug("stop addItem ");
		return SUCCESS;
	}

	public String removeItem(){
		logger.debug("start removeItem ");
		
		String itemId = this.httpServeltRequest.getParameter("itemId");
		logger.debug(" itemId {}",itemId);
		
		Map<Long,Integer> sessionCart = (Map<Long,Integer>)this.httpServeltRequest.getSession().getAttribute(SHOPPING_CART);
		
		sessionCart.remove(Long.valueOf(itemId));
		
		logger.debug("stop removeItem ");
		return SUCCESS;
	}	
	
	
	public String updateItemQuantity(){
		logger.debug("start updateItemQuantity ");
		
//		String itemId = this.httpServeltRequest.getParameter("itemId");
//		String quantity = this.httpServeltRequest.getParameter("itemId");
//		logger.debug(" itemId, quantity {}{}",itemId,quantity);

		logger.info(" shoppingCartItem = {}",shoppingCartItem);
		
		Map<Long,Integer> sessionCart = (Map<Long,Integer>)this.httpServeltRequest.getSession().getAttribute(SHOPPING_CART);		

		sessionCart.put(Long.valueOf(shoppingCartItem.getItemId()),Integer.valueOf(shoppingCartItem.getQuantity()));
		
		logger.debug("stop updateItemQuantity ");
		return SUCCESS;
	}	
	
	
	public String findShoppingCart() throws Exception {
		logger.debug("start getItems ");
		
		Map<Long,Integer> sessionCart = (Map<Long,Integer>)this.httpServeltRequest.getSession().getAttribute(SHOPPING_CART);

		finalCartDto = cartService.getCartItems(sessionCart);			

		
		return SUCCESS;
	}
	
	
	public String checkOut() throws Exception {
		logger.debug("start checkOut ");
		
		Map<Long,Integer> sessionCart = (Map<Long,Integer>)this.httpServeltRequest.getSession().getAttribute(SHOPPING_CART);

		finalCartDto = cartService.getCartItems(sessionCart);			

		return SUCCESS;
	}
	
	public String pay() throws Exception {
		logger.debug("start checkOut ");
		
		Map<Long,Integer> sessionCart = (Map<Long,Integer>)this.httpServeltRequest.getSession().getAttribute(SHOPPING_CART);

		CustomerDTO aCustomerDto = (CustomerDTO)this.httpServeltRequest.getSession().getAttribute(CUSTOMER_SESSION);		
		

		logger.debug("sessionCart {}",sessionCart);
		logger.debug("aCustomerDto.getId {}",aCustomerDto.getId());		
		
		orderService.createOrder(aCustomerDto.getId(), sessionCart);
		
		orders = orderService.findOrders(aCustomerDto.getId());
		
		this.httpServeltRequest.getSession().removeAttribute(SHOPPING_CART);
		
		return SUCCESS;
	}	
	
	public String empty(){
		logger.debug("start emptyCart ");

		Map<Long,Integer> sessionCart = (Map<Long,Integer>)this.httpServeltRequest.getSession().getAttribute(SHOPPING_CART);
		
		sessionCart.clear();
		
		logger.debug("stop emptyCart");
		return SUCCESS;
	}	
	

	@Override
	public void setServletRequest(HttpServletRequest arg0) {
		this.httpServeltRequest = arg0;
	}



	@Override
	public ShoppingCartItemDTO getModel() {
		return this.shoppingCartItem ;
	}
	
}

