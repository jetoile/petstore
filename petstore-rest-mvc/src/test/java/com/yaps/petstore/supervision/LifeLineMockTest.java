package com.yaps.petstore.supervision;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import javax.servlet.ServletContext;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.context.ContextConfiguration;

import com.yaps.petstore.web.rest.supervision.LifeLine;

@RunWith(MockitoJUnitRunner.class)
public class LifeLineMockTest {
	
	
	private static final String APPLICATION_VERSION = "1.0-SNAPSHOT";
	
	@InjectMocks
	LifeLine lifeline;

	@Mock
	ServletContext context;
	
	
	@Before
	public void setup() {
	        MockitoAnnotations.initMocks(this);
	        lifeline.setVersion(APPLICATION_VERSION);
	}	
	
    @Test
    public void testAdminVersion() throws Exception {

    	assertNotNull(lifeline);
    	
    	lifeline.adminVersion();
    	
    	assertNotNull(lifeline.getVersion());
    	assertEquals(APPLICATION_VERSION,lifeline.getVersion());
    }
    

	@Test
	public void testCheckDependencies() throws Exception {
		
		assertNotNull(lifeline);
		
		when(context.getRealPath("WEB-INF/lib")).thenReturn("src/test/resources/lib");
		
		String result =  lifeline.checkDependencies();
		

		assertTrue(result.contains("lib1.jar"));
		assertTrue(result.contains("lib2.jar"));
	}
	
    
}
