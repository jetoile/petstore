<%@ taglib prefix="s" uri="/struts-tags" %>

        <%--CENTRAL BODY--%>

            <P><strong>Sign In</strong></P>
            <P><strong>Are you a returning customer ?</strong></P>

            <TABLE cellSpacing=0 cellPadding=20 width="100%" border=1>
                <TR>
                    <TD vAlign=top width="50%">
                        <s:form    method="POST" theme="simple">
                            <TABLE  width="100%">
                                <TR>
                                    <TD align=middle colSpan="2"><B>Yes.</B></TD>
                                </TR>
                                <TR>
                                    <TD align=right><B>Customer Id (email):</B></TD>
                                    <TD><s:textfield size="15" name="email" /> </TD>
                                </TR>
                                <TR>
                                    <TD align=right><B>Password:</B></TD>
                                    <TD><s:password size="15" name="password"/></TD>
                                </TR>
                                <TR>
                                    <TD align=middle colSpan=2> 
                                    	<s:submit action="customer-sign-on" method="signOn"  value="Sign In"/>
                                    	</TD>
                                </TR>
                            </TABLE>
                        </s:form>
                    </TD>
                    <TD vAlign=top width="50%">
                        <s:form     method="POST" theme="simple">
                            <TABLE   width="100%">
                                <TR>
                                    <TD align=middle colSpan=2><B>No. I would like to sign up for an account.</B></TD>
                                </TR>
                                <TR>
                                    <TD align=right><B>Customer ID (email):</B></TD>
                                    <TD><s:textfield size="15"   name="email"/></TD>
                                </TR>
                                <TR>
                                    <TD align=right><B>Password:</B></TD>
                                    <TD><s:password size="15"   name="password"/></TD>
                                </TR>
                                <TR>
                                    <TD align=right><B>Password (Repeat):</B></TD>
                                    <TD><s:password size="15"  name="password2"/></TD>
                                </TR>
                                <TR>
                                    <TD align=middle colSpan=2>
                                    	<s:submit action="customer-create-form" method="createForm"  value="Create New Account"/>
                                    </TD>
                                </TR>
                            </TABLE>
                        </s:form>
                    </TD>
                </TR>
            </TABLE>


