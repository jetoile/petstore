package com.yaps.petstore.web.action;

import java.util.Collection;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.ActionSupport;
import com.yaps.petstore.model.CategoryDTO;
import com.yaps.petstore.model.ItemDTO;
import com.yaps.petstore.model.ProductDTO;
import com.yaps.petstore.service.catalog.CatalogService;

public class BusinessTreeAction extends ActionSupport  {

	/**
	 * 
	 */
	private static final long serialVersionUID = -450171797934520253L;


	Logger logger = LoggerFactory.getLogger(getCname());

	@Autowired
	private CatalogService catalogService;
	
	
	private Long categoryId;
	
	private Long productId;
	
	private Long itemId;	
	
	private ItemDTO itemDto;
	
	private String keyword;

	private List<CategoryDTO> categoryLst;	
	
	private List<ProductDTO> productLst;

	private List<ItemDTO> itemLst;	
	
	public List<ProductDTO> getProductLst(){
		return productLst;
	}

	
	public List<CategoryDTO> getCategoryLst() {
		return categoryLst;
	}

	public void setCategoryLst(List<CategoryDTO> catogoryLst) {
		this.categoryLst = catogoryLst;
	}



	public void setProductLst(List<ProductDTO> productLst){
		this.productLst = productLst;
	}	
	
	public Collection<ItemDTO> getItemLst(){
		return itemLst;
	}	
	
	public void setItemLst(List<ItemDTO> itemLst){
		this.itemLst = itemLst;
	}	

	
	public Long getProductId(){
		return productId;
	}

	public void setProductId(Long productId){
		this.productId = productId;
	}
	
	public Long getItemId() {
		return itemId;
	}

	public void setItemId(Long itemId){
		this.itemId = itemId;
	}
	
	
	public ItemDTO getItemDto(){
		return itemDto;
	}
	
	public void setItemDTO(ItemDTO itemDto){
		this.itemDto = itemDto;
	}
	
	public String getKeyword(){
		return keyword;
	}
	
	public void setKeyword(String keyword){
		 this.keyword = keyword;
	}	
	
    public void setCatalogService(CatalogService catalogService) {
		this.catalogService = catalogService;
	}

    public CatalogService getCatalogService() {
		return catalogService;
	}
   

    public void setCategoryId(Long categoryId){
    	this.categoryId = categoryId;
    }

	public Long getCategoryId() {
		return categoryId;
	}




    public String findCategories() throws Exception {
		logger.debug("avant CatalogDelegate.findCategories ");
    	
    	categoryLst = catalogService.findCategories();
		
	    return SUCCESS;
    }	
	
   
    public String findProducts() throws Exception {
		logger.debug("avant CatalogDelegate.findProducts "); 
		
    	productLst = catalogService.findProducts(categoryId);

	    return SUCCESS;
    }
    

    public String findItems() throws Exception {
		logger.debug("avant CatalogDelegate.findItems "); 
		
    	itemLst = catalogService.findItems(productId);

	    return SUCCESS;
    }
    
    
    public String searchItems() throws Exception {
		logger.debug("avant searchItems.findItems "); 
		
    	itemLst = catalogService.searchItems(keyword);

	    return SUCCESS;
    }    
    
    
    public String findItem() throws Exception {

    	itemDto = catalogService.findItem(itemId);
    	
	    return SUCCESS;
    }    

       
	protected String getCname() {
		return this.getClass().getName();
	}

    
}
